output "ecr_registries" {
    value = aws_ecr_repository.default.*.repository_url
}